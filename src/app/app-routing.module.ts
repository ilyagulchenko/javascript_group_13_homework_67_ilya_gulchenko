import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditMealComponent } from './meals/edit-meal/edit-meal.component';
import { AddListComponent } from './add-list/add-list.component';
import { HomePageComponent } from './home-page/home-page.component';
import {MealResolverService} from './meals/meal-resolver.service';

const routes: Routes = [
  {
    path: '', component: HomePageComponent, children: [
      {path: '', component: AddListComponent},
      {path: 'add', component: EditMealComponent},
      {path: 'edit/:id', component: EditMealComponent, resolve: {
          meal: MealResolverService
        }}
    ]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
