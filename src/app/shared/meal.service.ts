import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Subject} from 'rxjs';
import {Meal} from './meal.model';

@Injectable()

export class MealService {
  mealsFetch = new Subject<Meal[]>();
  total = 0;

  constructor(private http: HttpClient) {}

  private meals: Meal[] = [];

  fetchMeal(id: string) {
    return this.http.get<Meal | null>(`https://plovo-cc061-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      map(result => {
        if (!result) {
          return null;
        }
        return new Meal(id, result.time, result.description, result.calories);
      })
    )
  }

  fetchMeals() {
    this.total = 0;
    this.http.get<{[id: string]: Meal}>('https://plovo-cc061-default-rtdb.firebaseio.com/meals.json').pipe(
      map(result => {
        return Object.keys(result).map(id => {
          const mealData = result[id];

          return new Meal(
            id,
            mealData.time,
            mealData.description,
            mealData.calories,
          );
        });
      })
    ).subscribe(meals => {
      this.meals = meals;
      for (let i = 0; i < this.meals.length; i++) {
        this.total += this.meals[i].calories;
      }
      this.mealsFetch.next(this.meals.slice());
    })
  }

  getTotal() {
    return this.total;
  }

  addMeal(meal: Meal) {
    const body = {
      time: meal.time,
      description: meal.description,
      calories: meal.calories,
    };

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/meals.json', body);
  }

  editMeal(meal: Meal) {
    const body = {
      time: meal.time,
      description: meal.description,
      calories: meal.calories,
    }

    return this.http.put(`https://plovo-cc061-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body);
  }

  removeMeal(id: string) {
    return this.http.delete(`https://plovo-cc061-default-rtdb.firebaseio.com/meals/${id}.json`);
  }
}
