import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AddListComponent } from './add-list/add-list.component';
import { MealsComponent } from './meals/meals.component';
import { EditMealComponent } from './meals/edit-meal/edit-meal.component';
import { MealItemComponent } from './meals/meal-item/meal-item.component';
import { FormsModule } from '@angular/forms';
import { MealService } from './shared/meal.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AddListComponent,
    MealsComponent,
    EditMealComponent,
    MealItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
