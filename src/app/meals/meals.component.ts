import {Component, OnDestroy, OnInit} from '@angular/core';
import {Meal} from '../shared/meal.model';
import {MealService} from '../shared/meal.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit, OnDestroy {
  meals!: Meal[];
  mealsChangeSubscription!: Subscription;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
    this.mealsChangeSubscription = this.mealService.mealsFetch.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealService.fetchMeals();
  }

  ngOnDestroy(): void {
    this.mealsChangeSubscription.unsubscribe();
  }
}
