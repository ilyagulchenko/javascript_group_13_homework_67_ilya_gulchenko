import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MealService} from '../../shared/meal.service';
import {Meal} from '../../shared/meal.model';

@Component({
  selector: 'app-edit-meal',
  templateUrl: './edit-meal.component.html',
  styleUrls: ['./edit-meal.component.css']
})
export class EditMealComponent implements OnInit {
  @ViewChild('mealForm') mealForm!: NgForm;

  isEdit = false;
  editedId = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private mealService: MealService,
  ) {}

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data['meal'];

      if (meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          time: meal.time,
          description: meal.description,
          calories: meal.calories,
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          time: '',
          description: '',
          calories: '',
        });
      }
    });
  }

  setFormValue(value: {[key: string] : any}) {
    setTimeout(() => {
      this.mealForm.form.setValue(value);
    })
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(id, this.mealForm.value.time, this.mealForm.value.description, this.mealForm.value.calories);

    const next = () => {
      this.mealService.fetchMeals();
      void this.router.navigate([''], {relativeTo: this.route});
    };
    if (this.isEdit) {
      this.mealService.editMeal(meal).subscribe(next);
    } else {
      this.mealService.addMeal(meal).subscribe(next);
    }
  }

}
