import {Component, Input} from '@angular/core';
import {Meal} from '../../shared/meal.model';
import {MealService} from '../../shared/meal.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.css']
})
export class MealItemComponent {
  @Input() meal!: Meal;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private mealService: MealService,
  ) {
  }

  onRemove() {
    this.mealService.removeMeal(this.meal.id).subscribe(() => {
      this.mealService.fetchMeals();
    });
  }
}
