import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Meal} from '../shared/meal.model';
import {MealService} from '../shared/meal.service';
import {EMPTY, mergeMap, Observable, of} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class MealResolverService implements Resolve<Meal> {

  constructor(
    private router: Router,
    private mealService: MealService,
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never> {
    const mealId = <string>route.params['id'];

    return this.mealService.fetchMeal(mealId).pipe(mergeMap(meal => {
      if (meal) {
        return of(meal);
      }

      void this.router.navigate(['']);
      return EMPTY;
    }));
  }
}
