import { Component, OnInit } from '@angular/core';
import {MealService} from '../shared/meal.service';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent implements OnInit {

  constructor(private mealService: MealService) { }

  ngOnInit(): void {}

  getTotal() {
    return this.mealService.getTotal();
  }
}
